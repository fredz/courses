import { ITextbooks } from "./ITextbooks";

export interface ICourse {
	id: string;
	name: string;
	description: string;
	textbooks: ITextbooks[];
}