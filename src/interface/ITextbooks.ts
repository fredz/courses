
export interface ITextbooks {
   author: string;
   title: string;
}