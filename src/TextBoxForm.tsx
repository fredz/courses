import React from 'react';
import { FormControl, InputLabel, Input, IconButton } from '@material-ui/core';
import { ITextbooks } from './interface/ITextbooks';
import RotateLeftIcon from '@material-ui/icons/RotateLeft';

interface IState {
}

interface IProps {
   book: ITextbooks;
   key: number;
   index: number;
   handleChangeBook: (book: ITextbooks, index: number) => void
   handleResetBook: (index: number) => void
}

class TextBoxForm extends React.Component<IProps, IState> {


   handleChange = (event: React.ChangeEvent<HTMLInputElement>, name: string) => {
      const { index } = this.props;
      const newBook: any = this.props.book;

      const value = event.target.value;
      newBook[name + ""] = value;

      this.props.handleChangeBook(newBook, index);
   };

   handleReset = () => {
      const { index } = this.props;
      this.props.handleResetBook(index);
   };

   render() {
      const { book } = this.props;

      return (
         <div>
            <div>
               <IconButton
                  onClick={this.handleReset}
               >
                  <RotateLeftIcon />
               </IconButton>
            </div>
            <FormControl fullWidth >
               <InputLabel htmlFor="author">Author 3333333333333333333333333333</InputLabel>
               <Input
                  id="author"
                  value={book.author}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => { this.handleChange(event, "author") }}                 
               />
            </FormControl>

            <FormControl fullWidth >
               <InputLabel htmlFor="title">Title</InputLabel>
               <Input
                  id="title"
                  value={book.title}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => { this.handleChange(event, "title") }}                 
               />
            </FormControl>
            <br />
            <br />
            <br />
         </div >
      )
   }
}

export default TextBoxForm;