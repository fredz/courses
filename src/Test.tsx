import React from 'react';
import { Typography } from '@material-ui/core';
import AppHeader from './AppHeader';
import { Link } from "react-router-dom";

interface IState {   
}

interface IProps {
}

class Test extends React.Component<IProps, IState> {


   render() {      

      return (
         <div>
            <AppHeader />
            <Link to="/courseForm">course</Link>
            <Typography variant="h6" gutterBottom>Testing ..................................</Typography>            
         </div >
      )
   }
}

export default Test;