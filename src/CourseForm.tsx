import React from 'react';
import { TextField, Grid, Button, Typography } from '@material-ui/core';
import AppHeader from './AppHeader';
import { ICourse } from './interface/ICourse';
import { ITextbooks } from './interface/ITextbooks';
import TextBoxForm from './TextBoxForm';
import { CourseData } from './CourseData';
import { Link } from "react-router-dom";

interface IState {
   course: ICourse;
}

interface IProps {
}

class CourseForm extends React.Component<IProps, IState> {

   courseData = new CourseData();

   constructor(props: IProps) {
      super(props);
      this.state = {
         course: this.loadCourseData()  
      };
   }

   loadCourseData = () => {
      return this.courseData.getData()
   }

   handleChange = (event: React.ChangeEvent<HTMLInputElement>, name: string) => {
      const course: any = this.state.course;
      const value = event.target.value;

      course[name + ""] = value;

      this.setState({
         course: course
      })
   };

   handleChangeBook = (book: ITextbooks, index: number) => {
      const { course } = this.state;
      course.textbooks[index] = book;

      this.setState({
         course: course
      })
   };

   handleDiscard = () => {
      this.setState({
         course: this.loadCourseData()
      });
   }

   handleSubmit = (event: any) => {      
      this.courseData.setData(this.state.course);
      event.preventDefault();
   }

   handleResetBook = (index: number) => {
      const { course } = this.state;
      const oldCourse: ICourse = this.loadCourseData();

      course.textbooks[index] = oldCourse.textbooks[index]

      this.setState({
         course: course
      })
   }

   render() {
      const { course } = this.state;

      return (
         <div>
            <AppHeader />
            <Typography variant="h6" gutterBottom>Course 36</Typography>               


            <Link to="/test">Test</Link>
            <form onSubmit={this.handleSubmit}>
               <Grid container justify="center">
                  <Grid item xs={4}>
                     <Grid container justify="center">
                        <Grid item xs={12}>
                           <TextField
                              id="name"
                              label="Name"
                              fullWidth
                              margin="normal"
                              InputLabelProps={{
                                 shrink: true,
                              }}
                              value={course.name}
                              onChange={(event: React.ChangeEvent<HTMLInputElement>) => { this.handleChange(event, "name") }}
                           />
                           <TextField
                              id="dercription"
                              label="Dercription"
                              fullWidth
                              margin="normal"
                              InputLabelProps={{
                                 shrink: true,
                              }}
                              value={course.description}
                              onChange={(event: React.ChangeEvent<HTMLInputElement>) => { this.handleChange(event, "description") }}
                           />
                        </Grid>
                        <Grid item xs={12}>
                           <Typography variant="h6" gutterBottom>Text Books</Typography>
                        </Grid>
                        <Grid item xs={12}>
                           {course.textbooks.map((item: ITextbooks, index: number) => {
                              return (
                                 <TextBoxForm
                                    key={index}
                                    index={index}
                                    book={item}
                                    handleChangeBook={this.handleChangeBook}
                                    handleResetBook={this.handleResetBook}
                                 />
                              );
                           })}
                        </Grid>

                     </Grid>

                     <Grid container justify="center">
                        <Grid item xs={6}>
                           <Button
                              variant="contained"
                              color="primary"
                              onClick={this.handleDiscard}
                           >
                              Discard
                           </Button>
                        </Grid>
                        <Grid item xs={6}>
                           <Button
                              type="submit"
                              variant="contained"
                              color="primary"
                           >
                              Save
                           </Button>
                        </Grid>
                     </Grid>
                  </Grid>
               </Grid>
            </form>
         </div >
      )
   }
}

export default CourseForm;