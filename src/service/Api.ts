import axios from 'axios';
import { ApiConstants } from "./ApiConstants";

export const Api = {
   getCourses
}

const BASE_URL = 'http://localhost:3000';

function requestBaseGet(url: string, extraHeaders?: any, succesCallback?: any, errorCallback?: any) {
   requestBaseGetWithOptions(url, {}, extraHeaders, succesCallback, errorCallback);
}

function getRequestOptions(extraOptions?: any, extraHeaders?: any) {

   let requestOptions: any = {      
      timeout: 10000
   }

   if (requestOptions) {
      requestOptions = { ...requestOptions, ...extraOptions };
   }

   return requestOptions;
}

function requestBaseGetWithOptions(url: string, extraOptions?: any, extraHeaders?: any, succesCallback?: any, errorCallback?: any) {   
   axios
      .get(BASE_URL + url, getRequestOptions(extraOptions, extraHeaders))
      .then(response => {
         if (!(response.status === 200)) throw new Error(response.status + '');
         else return response.data;
      })
      .then(data => {
         
         if (succesCallback && typeof succesCallback === "function") {
            succesCallback(data);
         }
      })
      .catch(error => {
         if (errorCallback && typeof errorCallback === "function") {
            errorCallback(error);
         }
      });
}

function getCourses(success?: Function, error?: Function) {
   requestBaseGet(ApiConstants.getCourses, {}, success, error);
}
