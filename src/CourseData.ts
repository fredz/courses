import { ICourse } from "./interface/ICourse";

export class CourseData {

   data: ICourse;

   constructor() {
      this.data = {
         "id": "123",
         "name": "Introduction to Advertising",
         "description": "Learn about advertising",
         "textbooks": [
            {
               "author": "Joe Smith",
               "title": "Mobile Advertising Fundamentals"
            },
            {
               "author": "Eli Hinnegan",
               "title": "Introduction to Location-Based Advertising"
            },
            {
               "author": "Edward Bernays",
               "title": "Public Relations"
            }
         ]
      };
   }

   setData(newData : ICourse) {
      this.data = JSON.parse(JSON.stringify(newData))
   }

   getData() {
      return JSON.parse(JSON.stringify(this.data))
   }
}