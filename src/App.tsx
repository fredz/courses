import React from 'react';
import { Router, Route, Redirect } from 'react-router-dom'
import CourseForm from './CourseForm';
import { createBrowserHistory } from "history";
import Test from './Test';

import './App.css';

const customHistory = createBrowserHistory();

function App() {
   return (
      <div className="App">
         <Router history={customHistory} >
            <Route exact path="/" component={CourseForm} />
            <Route exact path="/courseForm" component={CourseForm} />
            <Route exact path="/test" component={Test} />            
            <Redirect from="*" to="/" />
         </Router>
      </div>
   );
}

export default App;